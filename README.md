# TASK MANAGER 1.0.0   
## Требования к software  
- Java 8  
- OpenJDK 11
- Apache Maven 3  
## Разработчик
Name: Yuliya Chernitsyna  
e-mail: chernitsyna_ys@nlmk.com  
## Команды для сборки приложения  
В папке проекта выполнить  
  
```maven clean``` - удаление всех собранных в процессе артефактов   
  
```maven package``` - cоздание .jar файла (+ компилирование, тестирование)  
## Команды для запуска приложения    
    java -jar task-manager-1.0.0.jar    